﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCharacterController : MonoBehaviour
{
    public float Speed;
    private float CanJump = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.time > CanJump)
            {
                GetComponent<Rigidbody>().AddForce(Vector3.up * 200);
                CanJump = Time.time + 1.4f;
            }
        }

    }

    void PlayerMovement()
    {

        float hor = Input.GetAxis("Vertical");
        float ver = Input.GetAxis("Horizontal");
        Vector3 playerMovement = new Vector3(-hor, 0f, ver) * Speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }

}
