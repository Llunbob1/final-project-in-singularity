﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Dialogue4 : MonoBehaviour
{

	public bool GuiOn;
	public string Text = "Sample";
	public Rect BoxSize = new Rect(0, 0, 200, 100);
	public GUISkin customSkin;

	void OnTriggerEnter()
	{
		GuiOn = true;
	}

	void OnTriggerExit()
	{
		GuiOn = false;
	}

	void OnGUI()
	{

		if (customSkin != null)
		{
			GUI.skin = customSkin;
		}

		if (GuiOn == true)
		{
			GUI.BeginGroup(new Rect((Screen.width - BoxSize.width) / 2, (Screen.height - BoxSize.height) / 2, BoxSize.width, BoxSize.height));

			GUI.Label(BoxSize, Text);

			GUI.EndGroup();

		}


	}

}
