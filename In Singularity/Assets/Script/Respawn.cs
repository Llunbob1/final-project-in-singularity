﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Respawn : MonoBehaviour
{

    public Transform teleportTarget;
    public GameObject Player;

    void OnTriggerEnter(Collider other)
    {

        Player.transform.position = teleportTarget.transform.position;

    }



}